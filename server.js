const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.get('/',(req,res)=>{
    
    res.status(200).send("working");
})
app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

 app.put('/update/:id', (req, res) =>{
  let updated = false;
   products.forEach((product) =>{
      
       if(product.id==req.params.id){
           product.productName = req.body.productName;
           product.price = req.body.price;
           updated=true;
          }
      
   });
     if(updated){
    res.status(200).send(`Product updated!`);    
   } else {
       res.status(404).send(`Could not find resource `);
   }

   
});

app.delete('/delete', (req, res) =>{
 let deleted=false;
  for(let i=0;i<products.length;i++){
   if(req.body.productName ==products[i].productName){
    products.splice(i, 1);   
    i--;
    deleted=true;
   
  }}
  if(deleted){
   res.status(200).send(`Product with name ${req.body.productName} was deleted`);
   } else {
    res.status(404).send(`Could not find any item with name ${req.body.productName}`);
   }
});

app.delete

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});